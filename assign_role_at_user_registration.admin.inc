<?php

/**
 * @file
 * Admin page callback for the assign_role_at_user_registration module
 */

/**
 * Builds and returns the module settings form
 */
function assign_role_at_user_registration_admin_settings() {
  $form['assign_role_at_user_registration_settings'] = array(
    '#type' => 'select',
    '#title' => t('Roles'),
    '#options' => user_roles($membersonly = TRUE, $permission = NULL),
    '#default_value' => variable_get('assign_role_at_user_registration_settings'),
    '#required' => TRUE,
  );

  $form['assign_role_at_user_registration_settings_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Text for the checkbox on the registration form'),
    '#default_value' => variable_get('assign_role_at_user_registration_settings_description', t('Do you want to belong to a certain role ?')),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
